package in.ac.iitb.ivrs.telephony.base.events;

import com.continuent.tungsten.commons.patterns.fsm.Event;

/**
 * Represents a disconnect telehpony event. This may be trigerred by a hangup from the user side or a disconnect from
 * the IVR side.
 */
public class DisconnectEvent extends Event<Object> {

	int duration;

	/**
	 * Create a new disconnect event.
	 * @param duration The total reported duration of the call.
	 */
	public DisconnectEvent(int duration) {
		super(null);
		this.duration = duration;
	}

	/**
	 * Returns the total duration of the call that was provided with the event.
	 * @return The total duration of the disconnected call.
	 */
	public int getDuration() {
		return duration;
	}

}
