package in.ac.iitb.ivrs.telephony.base.events;

import com.continuent.tungsten.commons.patterns.fsm.Event;

/**
 * Represents an event indicating that recording was finished.
 */
public class RecordEvent extends Event<Object> {

	String fileURL;
	int duration;

	/**
	 * Create a new record event.
	 * @param fileURL The filename of the recorded audio.
	 * @param duration The duration of the recorded audio.
	 */
	public RecordEvent(String fileURL, int duration) {
		super(null);
		this.fileURL = fileURL;
		this.duration = duration;
	}

	/**
	 * Returns the filename of the recorded audio.
	 * @return Filename of the recorded audio.
	 */
	public String getFileURL() {
		return fileURL;
	}

	/**
	 * Returns the duration of the recorded audio.
	 * @return Duration of the recorded audio.
	 */
	public int getDuration() {
		return duration;
	}

}
