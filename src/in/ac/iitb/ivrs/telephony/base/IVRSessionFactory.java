package in.ac.iitb.ivrs.telephony.base;

/**
 * An interface for objects that can create new session objects.
 */
public interface IVRSessionFactory {

	/**
	 * @see IVRSession#IVR(String, String, String, String, String, Class)
	 * @return A new IVR session.
	 * @throws Exception  
	 */
	public IVRSession createSession(String sessionId, String userNumber, String ivrNumber, String circle, String operator) throws Exception;

}
