package in.ac.iitb.ivrs.telephony.base.fsm;

import in.ac.iitb.ivrs.telephony.base.IVRSession;

import com.continuent.tungsten.commons.patterns.fsm.Action;
import com.continuent.tungsten.commons.patterns.fsm.FiniteStateException;
import com.continuent.tungsten.commons.patterns.fsm.Guard;
import com.continuent.tungsten.commons.patterns.fsm.State;
import com.continuent.tungsten.commons.patterns.fsm.StateTransitionMap;
import com.continuent.tungsten.commons.patterns.fsm.StateType;
import com.continuent.tungsten.commons.patterns.fsm.Transition;

/**
 * Extends the StateTransitionMap to provide utility functions for adding the states and transitions directly as
 * function calls.
 */
public class IVRStateTransitionMap extends StateTransitionMap<IVRSession> {

	/**
	 * Adds a new Start state to the map. There can be only one start state.
	 * @param name The name of the start state.
	 * @param exitAction The action to perform when exiting this state.
	 * @return The new start state that was created.
	 * @throws FiniteStateException
	 */
	public State<IVRSession> addStartState(String name, Action<IVRSession> exitAction) throws FiniteStateException {
		State<IVRSession> start = new State<IVRSession>(name, StateType.START, null, exitAction);
		this.addState(start);
		return start;
	}

	/**
	 * Adds a new End state to the map.
	 * @param name The name of the start state.
	 * @param entryAction The action to perform when entering this state.
	 * @return The new end state that was created.
	 * @throws FiniteStateException
	 */
	public State<IVRSession> addEndState(String name, Action<IVRSession> entryAction) throws FiniteStateException {
		State<IVRSession> end = new State<IVRSession>(name, StateType.END, entryAction, null);
		this.addState(end);
		return end;
	}

	/**
	 * Adds a new active telephony state with an exit action to the map.
	 * @param name Name of the new state (used for debugging purposes).
	 * @param parent Parent of the new state.
	 * @param entryAction The action to perform when entering this state.
	 * @param exitAction The action to perform when exiting this state.
	 * @return The new state that was created.
	 * @throws FiniteStateException 
	 */
	public State<IVRSession> addActiveState(String name, State<IVRSession> parent,
			Action<IVRSession> entryAction, Action<IVRSession> exitAction) throws FiniteStateException {

		State<IVRSession> state = new State<IVRSession>(name, StateType.ACTIVE, parent, entryAction, exitAction);
		this.addState(state);
		return state;
	}

	/**
	 * Adds a new active telephony state to the map.
	 * @param name Name of the new state (used for debugging purposes).
	 * @param parent Parent of the new state.
	 * @param entryAction The action to perform when entering this state.
	 * @return The new state that was created.
	 * @throws FiniteStateException 
	 */
	public State<IVRSession> addActiveState(String name, State<IVRSession> parent,
			Action<IVRSession> entryAction) throws FiniteStateException {

		return addActiveState(name, parent, entryAction, null);
	}

	/**
	 * Adds a new transition to the map.
	 * @param input The input state.
	 * @param guard The guard condition for the transition.
	 * @param output The output state.
	 * @param action The action to perform when executing this transition.
	 * @throws FiniteStateException
	 */
	public void allowTransition(State<IVRSession> input, Guard<IVRSession, Object> guard,
			State<IVRSession> output, Action<IVRSession> action) throws FiniteStateException {

		this.addTransition(new Transition<IVRSession, Object>(guard, input, action, output));
	}

}
